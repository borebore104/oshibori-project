import requests
import json
import base64
import openpyxl
import datetime
import time
import os
import shutil
import xml.etree.ElementTree as ET 
from openpyxl.styles import PatternFill
from urllib.parse import urljoin

MY_URL: str = "https://nanasedo.com/"
MY_USER: str = "sedorinana"
MY_APP_PASSWORD: str = "9mrP JawT HgKv 4ODP IYBo IdXJ"

BLOG_ID: int = ''
BLOG_TITLE: str = ''
BLOG_GENRE: str = ''

date = ""
global_rakutenApiId = ''
global_raku_afiid = ""
global_ama_afiid = ""
global_exclusionTitleList = []
global_exclusionShopList = []
global_minOtoku = 0
global_maxOtoku = 0

icon_raku = "https://nanasedo.com/wp-content/uploads/2021/08/rakuten.png"
icon_ama = "https://nanasedo.com/wp-content/uploads/2021/08/Amazon.png"
debug = True

#---------------------------- Price -------------------------------------------------------------------

def validReadData(title, image, amaPrice, asin, jan):
    if not title:
        print('title is None')
        return False
    elif not image:
        print('image is None')
        return False
    elif not amaPrice:
        print('amaPrice is None')
        return False
    elif not asin:
        print('asin is None')
        return False
    elif not jan:
        print('jan is None')
        return False
    return True

def uniqueImage(url):
    index = url.find(';')
    if index == -1:
        return url
    return url[:+index]

def loadJan():
    global date
    global global_raku_afiid
    global global_ama_afiid
    global global_minOtoku
    global global_maxOtoku
    
    loadConfig()

    readWb = openpyxl.load_workbook('work\source' + BLOG_GENRE + '.xlsx') #ProductFinderのExportファイル
    readWs = readWb['Sheet0'] #ProductFinderでExportしたらSheet0になる

    writeWbName = str('work\\price_dest' + BLOG_GENRE + '.xlsx')
    writeWb = openpyxl.Workbook()
    writeWb.save(writeWbName)
    writeWs = writeWb['Sheet']

    writeCnt = 1 #書き込みExcelのインデックス
    for readCnt in range(2,10000):
        title = readWs['C' + str(readCnt)].value
        image = readWs['B' + str(readCnt)].value
        amaPrice = readWs['V' + str(readCnt)].value
        asin = readWs['DF'+str(readCnt)].value
        jan = readWs['DG'+str(readCnt)].value

        if not title:
            break
        if validReadData(title, image, amaPrice, asin, jan) is False: #ここで空欄とか色々条件追加が必要
            print("$$$Continue$$$ validReadData error!!")
            continue

        title = str(title)
        image = uniqueImage(str(image))
        amaPrice = str(amaPrice)
        asin = str(asin)
        jan = str(jan)
        jan = jan[:13]

        print("[start] :" + str(readCnt))
        time.sleep(1)
        item = kick_rakten_api(jan)
        if item is None:
            print('$$$Continue$$$ rakuten no item')
            continue

        shopName = item['shopName']
        if not shopName:
            shopName = ''
        shopName = str(shopName)

        amaPrice = int(float(amaPrice))
        amaUrl = "https://www.amazon.co.jp/dp/" + asin + global_ama_afiid
        rakuPrice = int(item["itemPrice"])
        rakuUrl = str(item["affiliateUrl"])
        print('$$$$$DEBUG$$$$$ affiliate!!!')

        otoku = float(amaPrice - rakuPrice) / float(amaPrice)
        if otoku < global_minOtoku or otoku > global_maxOtoku:
            print('$$$Continue$$$ price off shortage:' + str(otoku))
            continue
        
        print(title)
        writeWs.cell(row=writeCnt, column=1).value = title
        print(image)
        writeWs.cell(row=writeCnt, column=2).value = image
        print(amaPrice)
        writeWs.cell(row=writeCnt, column=3).value = amaPrice
        print(amaUrl)
        writeWs.cell(row=writeCnt, column=4).value = amaUrl
        print(rakuPrice)
        writeWs.cell(row=writeCnt, column=5).value = rakuPrice
        print(rakuUrl)
        writeWs.cell(row=writeCnt, column=6).value = rakuUrl
        print(shopName)
        writeWs.cell(row=writeCnt, column=7).value = shopName
        print(otoku)
        writeWs.cell(row=writeCnt, column=8).value = otoku
        writeCnt += 1

        #100件書いたら保存
        if writeCnt % 5 == 0:
            writeWb.save(writeWbName)
            print('excel saved.')

    writeWb.save(writeWbName)
    writeWb.close()
    readWb.close()
    print('excel saved.')

def kick_rakten_api(jan):
    REQUEST_URL = "https://app.rakuten.co.jp/services/api/IchibaItem/Search/20170706"
    search_param = set_api_parameter(jan)
    response = requests.get(REQUEST_URL, search_param)
    result = response.json()

    try:
        # 楽天に商品がない場合
        if result['pageCount'] == 0:
            print('pageCnt = 0')
            return None
    except KeyError:
        return None

    #安い順にループして問題ない店を探す
    for i in result["Items"]:
        shopName = i['Item']['shopName']
        if not shopName:
            shopName = ''
        shopName = str(shopName)
        if validShopName(shopName) is False:
            print('$$$Continue$$$ rakuten shop is bad : ' + shopName)
            continue
        if validItemName(str(i['Item']['itemName'])):
            item = i["Item"]
            print('item find.')
            return item
    print('ゴミしかなかった。')
    return None

def set_api_parameter(jan):
    global global_rakutenApiId
    global global_raku_afiid
    parameter = {
          "format"        : "json"
        , "keyword"       : jan
        , "applicationId" : [global_rakutenApiId]
        , "affiliateId" : global_raku_afiid
        , "availability"  : 1
        , "sort"          : "+itemPrice"
        }
    return parameter


def validItemName(name):
    global global_exclusionTitleList
    for exclusion in global_exclusionTitleList:
        if exclusion in name:
            print('title is invalid:[' + exclusion + ']')
            return False
    return True

def validShopName(name):
    global global_exclusionShopList
    for exclusion in global_exclusionShopList:
        if exclusion in name:
            print('shop is invalid:[' + exclusion + ']')
            return False
    return True

def set_api_parameter(jan):
    global global_rakutenApiId
    global global_raku_afiid
    parameter = {
          "format"        : "json"
        , "keyword"       : jan
        , "applicationId" : [global_rakutenApiId]
        , "affiliateId" : global_raku_afiid
        , "availability"  : 1
        , "sort"          : "+itemPrice"
        }
    return parameter

def loadConfig():
    global global_rakutenApiId
    global global_raku_afiid
    global global_ama_afiid
    global global_exclusionTitleList
    global global_exclusionShopList
    global global_minOtoku
    global global_maxOtoku
    tree = ET.parse('config.xml') 
    root = tree.getroot()
    for name in root.iter('rakuten'):
        global_rakutenApiId = name.text
        break
    for name in root.iter('rakuafi'):
        global_raku_afiid = name.text
        break
    for name in root.iter('amaafi'):
        global_ama_afiid = name.text
        break
    for name in root.iter('exclusionTitle'):
        global_exclusionTitleList.append(name.text)
    print('exclusionTitle:' + str(global_exclusionTitleList))
    for name in root.iter('exclusionShop'):
        global_exclusionShopList.append(name.text)
    print('exclusionShop:' + str(global_exclusionShopList))
    for name in root.iter('minOtoku'):
        global_minOtoku = float(name.text)
        break
    for name in root.iter('maxOtoku'):
        global_maxOtoku = float(name.text)
        break
    print('minOtoku : ' + str(global_minOtoku) + ',  maxOtoku : ' + str(global_maxOtoku))

def loadDate():
    global date
    tmp = datetime.datetime.now()
    date = tmp.strftime('%Y/%m/%d %H:%M')
    print('CreateDate:' + str(date))

#---------------------------- CreateTable -------------------------------------------------------------------

def createImage(url):
  tag = '<img src=\"' + url + '\" style=\"max-width: 90%\">'
  return str(tag)

def createAmaLink(price, url):
  global icon_ama
  global ama_afiid
  tag = '\\ ' + str("{:,}".format(price)) + '<br><a href=\"' + url + '\" target=\"_blank\" rel=\"noopener\"> <img src=\"' + icon_ama + '\"></a>'
  return str(tag)

def createRakuLink(price, url):
  global icon_raku
  tag = '\\ ' + str("{:,}".format(price)) + '<br><a href=\"' + url + '\" target=\"_blank\" rel=\"noopener\"> <img src=\"' + icon_raku + '\"></a>'
  return str(tag)

def createRow(title, imageUrl, amaPrice, amaUrl, rakuPrice, rakuUrl, shopName, discount):
  tag  = '  <tr ' + getRowColor(int(discount * 100)) + '>\n'
  tag += '    <td align=\"left\" valign=\"middle\">' + title + '</td>\n'
  tag += '    <td>' + createImage(imageUrl) + '</td>\n'
  tag += '    <td align=\"middle\" valign=\"middle\">' + createAmaLink(amaPrice, amaUrl) + '</td>\n'
  tag += '    <td align=\"middle\" valign=\"middle\">' + createRakuLink(rakuPrice, rakuUrl) + '</td>\n'
  tag += '    <td align=\"middle\" valign=\"middle\">' + str(int(discount * 100)) + '％OFF' + '</td>\n'
  tag += '    <td align=\"left\" valign=\"middle\">' + shopName + '</td>\n'
  tag += '  </tr>\n'
  return str(tag)

def getRowColor(discount):
  if discount > 40:
    return str('bgcolor=\"#f7c6bd\"')
  elif discount > 30:
    return str('bgcolor=\"#eaf6fd\"')
  else:
    return str('')

def createTableHeader():
  global date
  tag  = ''
  tag += '<!-- wp:buttons {\"contentJustification\":\"center\",\"align\":\"wide\",\"className\":\"btn\u002d\u002dgreen\"} --><div class=\"wp-block-buttons alignwide is-content-justification-center btn--green\">'
  tag += '<!-- wp:button {\"backgroundColor\":\"pink\",\"textColor\":\"white\",\"width\":25,\"style\":{\"border\":{\"radius\":10}},\"className\":\"is-style-outline\"} -->'
  tag += '<div class=\"wp-block-button has-custom-width wp-block-button__width-25 is-style-outline\"><a class=\"wp-block-button__link has-white-color has-pink-background-color has-text-color has-background\" href=\"https://nanasedo.com/2021/09/26/%e6%a5%bd%e5%a4%a9%e3%81%8a%e8%b2%b7%e3%81%84%e5%be%97%e5%95%86%e5%93%81%e3%83%aa%e3%82%b9%e3%83%88dvd/\" style=\"border-radius:10px\" target=\"_blank\" rel=\"noreferrer noopener\">DVD</a></div>'
  tag += '<!-- /wp:button -->'
  tag += '<!-- wp:button {\"backgroundColor\":\"key-color\",\"textColor\":\"white\",\"width\":25,\"style\":{\"border\":{\"radius\":10}},\"className\":\"is-style-outline\"} -->'
  tag += '<div class=\"wp-block-button has-custom-width wp-block-button__width-25 is-style-outline\"><a class=\"wp-block-button__link has-white-color has-key-color-background-color has-text-color has-background\" href=\"https://nanasedo.com/2021/09/26/%e6%a5%bd%e5%a4%a9%e3%81%8a%e8%b2%b7%e3%81%84%e5%be%97%e5%95%86%e5%93%81%e3%83%aa%e3%82%b9%e3%83%88%e3%81%8a%e3%82%82%e3%81%a1%e3%82%83/\" style=\"border-radius:10px\" target=\"_blank\" rel=\"noreferrer noopener\">おもちゃ</a></div>'
  tag += '<!-- /wp:button -->'
  tag += '<!-- wp:button {\"backgroundColor\":\"purple\",\"textColor\":\"white\",\"width\":25,\"style\":{\"border\":{\"radius\":10}},\"className\":\"is-style-outline\"} -->'
  tag += '<div class=\"wp-block-button has-custom-width wp-block-button__width-25 is-style-outline\"><a class=\"wp-block-button__link has-white-color has-purple-background-color has-text-color has-background\" href=\"https://nanasedo.com/2021/09/26/%e6%a5%bd%e5%a4%a9%e3%81%8a%e8%b2%b7%e3%81%84%e5%be%97%e5%95%86%e5%93%81%e3%83%aa%e3%82%b9%e3%83%88%e3%82%b2%e3%83%bc%e3%83%a0/\" style=\"border-radius:10px\" target=\"_blank\" rel=\"noreferrer noopener\">ゲーム</a></div>'
  tag += '<!-- /wp:button -->'
  tag += '<!-- wp:button {\"backgroundColor\":\"brown\",\"textColor\":\"white\",\"width\":25,\"style\":{\"border\":{\"radius\":10}},\"className\":\"is-style-outline\"} -->'
  tag += '<div class=\"wp-block-button has-custom-width wp-block-button__width-25 is-style-outline\"><a class=\"wp-block-button__link has-white-color has-brown-background-color has-text-color has-background\" href=\"https://nanasedo.com/2021/09/26/%e6%a5%bd%e5%a4%a9%e3%81%8a%e8%b2%b7%e3%81%84%e5%be%97%e5%95%86%e5%93%81%e3%83%aa%e3%82%b9%e3%83%88%e3%83%9b%e3%83%93%e3%83%bc/\" style=\"border-radius:10px\" target=\"_blank\" rel=\"noreferrer noopener\">ホビー</a></div>'
  tag += '<!-- /wp:button -->'
  tag += '<!-- wp:button {\"backgroundColor\":\"orange\",\"textColor\":\"white\",\"width\":25,\"style\":{\"border\":{\"radius\":10}},\"className\":\"is-style-outline\"} -->'
  tag += '<div class=\"wp-block-button has-custom-width wp-block-button__width-25 is-style-outline\"><a class=\"wp-block-button__link has-white-color has-orange-background-color has-text-color has-background\" href=\"https://nanasedo.com/2021/09/26/%e6%a5%bd%e5%a4%a9%e3%81%8a%e8%b2%b7%e3%81%84%e5%be%97%e5%95%86%e5%93%81%e3%83%aa%e3%82%b9%e3%83%88%e5%ae%b6%e9%9b%bb%ef%bc%86%e3%82%ab%e3%83%a1%e3%83%a9/\" style=\"border-radius:10px\" target=\"_blank\" rel=\"noreferrer noopener\">家電＆カメラ</a></div>'
  tag += '<!-- /wp:button -->'
  tag += '<!-- wp:button {\"backgroundColor\":\"red\",\"textColor\":\"white\",\"width\":25,\"style\":{\"border\":{\"radius\":10}},\"className\":\"is-style-outline\"} -->'
  tag += '<div class=\"wp-block-button has-custom-width wp-block-button__width-25 is-style-outline\"><a class=\"wp-block-button__link has-white-color has-red-background-color has-text-color has-background\" href=\"https://nanasedo.com/2021/09/26/%e6%a5%bd%e5%a4%a9%e3%81%8a%e8%b2%b7%e3%81%84%e5%be%97%e5%95%86%e5%93%81%e3%83%aa%e3%82%b9%e3%83%88%e5%a4%a7%e5%9e%8b%e5%ae%b6%e9%9b%bb/\" style=\"border-radius:10px\" target=\"_blank\" rel=\"noreferrer noopener\">DIY・工具</a></div>'
  tag += '<!-- /wp:button -->'
  tag += '<!-- wp:button {\"backgroundColor\":\"blue\",\"textColor\":\"white\",\"width\":25,\"style\":{\"border\":{\"radius\":10}},\"className\":\"is-style-outline\"} -->'
  tag += '<div class=\"wp-block-button has-custom-width wp-block-button__width-25 is-style-outline\"><a class=\"wp-block-button__link has-white-color has-blue-background-color has-text-color has-background\" href=\"https://nanasedo.com/2021/09/26/%e6%a5%bd%e5%a4%a9%e3%81%8a%e8%b2%b7%e3%81%84%e5%be%97%e5%95%86%e5%93%81%e3%83%aa%e3%82%b9%e3%83%88%e3%82%b9%e3%83%9d%e3%83%bc%e3%83%84%ef%bc%86%e3%82%a2%e3%82%a6%e3%83%88%e3%83%89%e3%82%a2/\" style=\"border-radius:10px\" target=\"_blank\" rel=\"noreferrer noopener\">スポーツ</a></div>'
  tag += '<!-- /wp:button -->'
  tag += '<!-- wp:button {\"backgroundColor\":\"teal\",\"textColor\":\"white\",\"width\":25,\"style\":{\"border\":{\"radius\":10}},\"className\":\"is-style-outline\"} -->'
  tag += '<div class=\"wp-block-button has-custom-width wp-block-button__width-25 is-style-outline\"><a class=\"wp-block-button__link has-white-color has-teal-background-color has-text-color has-background\" href=\"https://nanasedo.com/2021/09/26/%e6%a5%bd%e5%a4%a9%e3%81%8a%e8%b2%b7%e3%81%84%e5%be%97%e5%95%86%e5%93%81%e3%83%aa%e3%82%b9%e3%83%88%e3%83%91%e3%82%bd%e3%82%b3%e3%83%b3%e3%83%bb%e5%91%a8%e8%be%ba%e6%a9%9f%e5%99%a8/\" style=\"border-radius:10px\" target=\"_blank\" rel=\"noreferrer noopener\">PC周辺機器</a></div>'
  tag += '<!-- /wp:button -->'
  tag += '</div><!-- /wp:buttons -->'
  tag += '<h1>更新日時：' + date + '</h1><br>'
  tag += '<table style=\"width:100%; border-collapse: collapse; table-layout: fixed;\"  border=\"1\">\n'
  tag += '  <thead>\n'
  tag += '    <tr>\n'
  tag += '      <td style=\"width:40%\" align=\"middle\" valign=\"middle\">タイトル</td>\n'
  tag += '      <td style=\"width:10%\" align=\"middle\" valign=\"middle\">イメージ</td>\n'
  tag += '      <td style=\"width:15%\" align=\"middle\" valign=\"middle\">Amazon</td>\n'
  tag += '      <td style=\"width:17%\" align=\"middle\" valign=\"middle\">楽天</td>\n'
  tag += '      <td style=\"width:8%\" align=\"middle\" valign=\"middle\">割引率</td>\n'
  tag += '      <td style=\"width:10%\" align=\"middle\" valign=\"middle\">ショップ</td>\n'
  tag += '    </tr>\n'
  tag += '  </thead>\n'
  tag += '  <tbody>\n'
  return str(tag)

# def createTableHeader():
#   tag  = '<table style=\"width:100%; border-collapse: collapse; table-layout: fixed;\"  border=\"1\">\n'
#   tag += '  <thead>\n'
#   tag += '    <tr>\n'
#   tag += '      <td style=\"width:40%\" align=\"middle\" valign=\"middle\">タイトル</td>\n'
#   tag += '      <td style=\"width:10%\" align=\"middle\" valign=\"middle\">イメージ</td>\n'
#   tag += '      <td style=\"width:15%\" align=\"middle\" valign=\"middle\">Amazon</td>\n'
#   tag += '      <td style=\"width:17%\" align=\"middle\" valign=\"middle\">楽天</td>\n'
#   tag += '      <td style=\"width:8%\" align=\"middle\" valign=\"middle\">割引率</td>\n'
#   tag += '      <td style=\"width:10%\" align=\"middle\" valign=\"middle\">ショップ</td>\n'
#   tag += '    </tr>\n'
#   tag += '  </thead>\n'
#   tag += '  <tbody>\n'
#   return str(tag)


def createTableHooter():
  tag  = '  </tbody>\n'
  tag += '</table>\n'
  return str(tag)

def createTable():
  wb = openpyxl.load_workbook('work\\price_dest' + BLOG_GENRE + '.xlsx')
  ws = wb["Sheet"]
  print('createHeader end.')

  file = open('work\\createTable_dest' + BLOG_GENRE + '.html', 'a', encoding='UTF-8')
  file.write(createTableHeader())
  for rowCnt in range(1,1000):
    print(rowCnt)
    title = ws['A' + str(rowCnt)].value
    if not title:
      break 
    print(title)
    imageUrl = ws['B' + str(rowCnt)].value
    print(imageUrl)
    amaPrice = ws['C' + str(rowCnt)].value
    print(amaPrice)
    amaUrl = ws['D' + str(rowCnt)].value
    print(amaUrl)
    rakuPrice = ws['E' + str(rowCnt)].value
    print(rakuPrice)
    rakuUrl = ws['F' + str(rowCnt)].value
    print(rakuUrl)
    shopName = ws['G' + str(rowCnt)].value
    print(shopName)
    discount = ws['H' + str(rowCnt)].value
    print(discount)
    row = createRow(title, imageUrl, amaPrice, amaUrl, rakuPrice, rakuUrl, shopName, discount)
    file.write(row)
  file.write(createTableHooter())
  file.close()


#---------------------------- AutoReload -------------------------------------------------------------------

def wp_update_post(contentId, content) -> dict:
    credentials = MY_USER + ':' + MY_APP_PASSWORD
    token = base64.b64encode(credentials.encode())
    headers = {'Authorization': 'Basic ' + token.decode('utf-8')}

    post = {
               'title': BLOG_TITLE,
               'status': 'publish',
               'content': content,
            }

    post_id: int = int(contentId)
    res = requests.post(f"{MY_URL}/wp-json/wp/v2/posts/{post_id}", headers=headers, json=post)
    if res.ok:
        print("投稿の更新 成功 code:{res.status_code}")
        return json.loads(res.text)
    else:
        print(f"投稿の更新 失敗 code:{res.status_code} reason:{res.reason} msg:{res.text}")
        return {}

#------------------------------ DeleteFile -------------------------------------------------

def deleteFile():
  shutil.rmtree('work')
  os.mkdir('work')
  return


def rootine():
  global BLOG_ID
  global BLOG_TITLE
  global BLOG_GENRE
  tree = ET.parse('slnconfig.xml') 
  root = tree.getroot()
  for child in root:
    BLOG_ID = int(child.attrib['id'])
    BLOG_TITLE = str(child.find('title').text)
    BLOG_GENRE = str(child.find('genre').text)
    loadJan()
    loadDate()
    createTable()
    f = open('work\\createTable_dest' + BLOG_GENRE + '.html', 'r', encoding='UTF-8')
    data = f.read()
    wp_update_post(BLOG_ID, str(data))
    time.sleep(10)
    f.close()
rootine()
deleteFile()







