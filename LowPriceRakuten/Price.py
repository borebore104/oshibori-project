import requests
import openpyxl
import datetime
import time
from openpyxl.styles import PatternFill
import xml.etree.ElementTree as ET 

date = ""
global_rakutenApiId = ''
global_raku_afiid = ""
global_ama_afiid = ""
global_exclusionTitleList = []



def validReadData(title, image, amaPrice, asin, jan):
    if title is None:
        print('title is None')
        return False
    elif image is None:
        print('image is None')
        return False
    elif amaPrice is None:
        print('amaPrice is None')
        return False
    elif asin is None:
        print('asin is None')
        return False
    elif jan is None:
        print('jan is None')
        return False
    return True

def uniqueImage(url):
    index = url.find(';')
    if index == -1:
        return url
    return url[:+index]

def loadJan():
    global date
    global global_raku_afiid
    global global_ama_afiid
    loadConfig()

    readWb = openpyxl.load_workbook("source.xlsx")
    readWs = readWb["Sheet0"]

    writeWbName = str('dest_' + date + '.xlsx')
    writeWb = openpyxl.Workbook()
    writeWb.save(writeWbName)
    writeWs = writeWb['Sheet']

    writeCnt = 1 #書き込みExcelのインデックス
    for readCnt in range(2,10000):
        title = readWs['C' + str(readCnt)].value
        image = readWs['B' + str(readCnt)].value
        amaPrice = readWs['U' + str(readCnt)].value
        asin = readWs['DF'+str(readCnt)].value
        jan = readWs['DG'+str(readCnt)].value

        if not title:
            break
        if validReadData(title, image, amaPrice, asin, jan) is False: #ここで空欄とか色々条件追加が必要
            print("validReadData error continue!!")
            continue

        title = str(title)
        image = uniqueImage(str(image))
        amaPrice = str(amaPrice)
        asin = str(asin)
        jan = str(jan)
        jan = jan[:13]

        print("[start] :" + str(readCnt))
        time.sleep(1)
        item = kick_rakten_api(jan)
        if item is None:
            print('rakuten no item')
            continue
        amaPrice = int(amaPrice)
        amaUrl = "https://www.amazon.co.jp/dp/" + asin + global_ama_afiid
        rakuPrice = int(item["itemPrice"])
        rakuUrl = str(item["itemUrl"])

        otoku = float(rakuPrice) / float(amaPrice)
        if otoku < 0.5 or otoku > 0.8:
            print('お得率不正:' + str(otoku))
            continue
        
        print(title)
        writeWs.cell(row=writeCnt, column=1).value = title
        print(image)
        writeWs.cell(row=writeCnt, column=2).value = image
        print(amaPrice)
        writeWs.cell(row=writeCnt, column=3).value = amaPrice
        print(amaUrl)
        writeWs.cell(row=writeCnt, column=4).value = amaUrl
        print(rakuPrice)
        writeWs.cell(row=writeCnt, column=5).value = rakuPrice
        print(rakuUrl)
        writeWs.cell(row=writeCnt, column=6).value = rakuUrl
        writeCnt += 1

        #100件書いたら保存
        if writeCnt % 5 == 0:
            writeWb.save(writeWbName)
            print('excel saved.')

    writeWb.save(writeWbName)
    print('excel saved.')




def kick_rakten_api(jan):
    REQUEST_URL = "https://app.rakuten.co.jp/services/api/IchibaItem/Search/20170706"
    search_param = set_api_parameter(jan)
    response = requests.get(REQUEST_URL, search_param)
    result = response.json()

    # 楽天に商品がない場合
    if result['pageCount'] == 0:
        print('pageCnt = 0')
        return None
    # 楽天に商品がある場合
    else:
        #安い順にループして問題ない店を探す
        for i in result["Items"]:
            if validItemName(str(i['Item']['itemName'])):
               item = i["Item"]
               print('item find.')
               return item
        print('ゴミしかなかった。')
        return None

def validItemName(name):
    global global_exclusionTitleList
    for exclusion in global_exclusionTitleList:
        if exclusion in name:
            print('title is invalid:[' + exclusion + ']')
            return False
    return True

def set_api_parameter(jan):
    global global_rakutenApiId
    global global_raku_afiid
    parameter = {
          "format"        : "json"
        , "keyword"       : jan
        , "applicationId" : [global_rakutenApiId]
        , "affiliateId" : global_raku_afiid
        , "availability"  : 1
        , "sort"          : "+itemPrice"
        }
    return parameter

def loadConfig():
    global global_rakutenApiId
    global global_raku_afiid
    global global_ama_afiid
    global global_exclusionTitleList
    tree = ET.parse('config.xml') 
    root = tree.getroot()
    for name in root.iter('rakuten'):
        global_rakutenApiId = name.text
        break
    for name in root.iter('rakuafi'):
        global_raku_afiid = name.text
        break
    for name in root.iter('amaafi'):
        global_ama_afiid = name.text
        break
    for name in root.iter('exclusionTitle'):
        global_exclusionTitleList.append(name.text)
    print('exclusionTitle:' + str(global_exclusionTitleList))

def loadDate():
    global date
    tmp = datetime.datetime.now()
    date = tmp.strftime('%Y%m%d%H%M%S')
    print('CreateDate:' + str(date))

loadDate()
loadJan()
