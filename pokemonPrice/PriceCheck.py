import requests
import openpyxl
import datetime
import time
from openpyxl.styles import PatternFill
import xml.etree.ElementTree as ET 
from bs4 import BeautifulSoup


load_url = "https://www.cardrush-pokemon.jp/product/"
debug = True
timestamp = ''



def execute():
  #日付文字列取得
  now = datetime.datetime.now()
  datestr = now.strftime('%y%m%d')
  timestamp = now.strftime('%y/%m/%d %H:%M:%S')
  print(datestr)

  #読み込みExcelを開く
  loadwb = openpyxl.load_workbook("cardlist.xlsx")
  loadws = loadwb["Sheet1"]

  #書き込みExcelを開く
  writewb = openpyxl.load_workbook("cardlist2.xlsx")
  writews = writewb["Sheet1"]

  file = open('deleteList.txt', 'a', encoding='UTF-8')

  createHead(writewb, writews)


  #読み込みExcelのID文ループ
  rowIndex = 5001
  for row in loadws["A5396:A30000"]:
    rowIndex +=1
    urlid = str(row[0].value)
    if urlid == 'None':
      print("もう終わり")
      break
    html = requests.get(str(load_url) + urlid)
    soup = BeautifulSoup(html.content, "html.parser")
    if soup.find(class_="nftitle_bottom") is None:
      writews.cell(row=rowIndex, column=1).value = int(urlid)
      writews.cell(row=rowIndex, column=2).value = timestamp
      writews.cell(row=rowIndex, column=3).value = getPrice(soup)
      writews.cell(row=rowIndex, column=4).value = getStock(soup)
      if debug: print(urlid)
      if debug: print(timestamp)
      if debug: print(getPrice(soup))
      if debug: print(getStock(soup))
    #あるはずなのに無い場合は削除リストに追加
    else:
      print("削除されてる："+ urlid)
      file.write(urlid + '\n')

    #たまには保存
    if rowIndex % 1000 == 0:
      writewb.save("cardlist2.xlsx")
      print('excel saved.')
    time.sleep(1)

  #最後は保存
  writewb.save("cardlist2.xlsx")
  return 

def createHead(wb,ws):
  ws.cell(row=1, column=1).value = 'UrlId'
  ws.cell(row=1, column=2).value = 'Date'
  ws.cell(row=1, column=3).value = 'Price'
  ws.cell(row=1, column=4).value = 'Stock'
  wb.save("cardlist2.xlsx")
  print("createHead end.")
  return 

def getPrice(soup):
  if soup.find(id = "pricech") is None:
    return 0
  return int(soup.find(id = "pricech").text.replace(',','').replace('円',''))

def getStock(soup):
  if soup.find(class_="detail_section stock") is None:
    return 0
  stocktxt = soup.find(class_="detail_section stock").text
  if stocktxt == str("×"):
    return 0
  else:
    return int(str(stocktxt).replace('在庫数','').replace('枚','').replace('点','').replace('個','').replace('箱','').replace(',','').rstrip().strip())

#  if str(convertStock(soup.find(class_="detail_section stock").text)) == str("×"):
#  return str(soup.find(class_="detail_section stock").text)).replace('在庫数','').replace('枚','').rstrip().strip()


execute()


