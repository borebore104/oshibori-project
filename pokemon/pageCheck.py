import requests
import datetime
import openpyxl
import time
from bs4 import BeautifulSoup
from urllib.request import urlopen

load_url = "https://www.cardrush-pokemon.jp/product/"

def execute():
  global load_url
  wb = openpyxl.load_workbook("cardlist.xlsx")
  ws = wb["Sheet1"]


  dt_now = datetime.datetime.now()
  print(dt_now.strftime('%y/%m/%d %H:%M:%S'))
  lineCnt = 2
  for num in range(100000):
    html = requests.get(str(load_url) + str(num))
    soup = BeautifulSoup(html.content, "html.parser")
    if soup.find(class_="nftitle_bottom") is None:
      print("find ; " + str(num))
      ws.cell(row=lineCnt, column=1, value=str(num))
      lineCnt += 1
      if lineCnt % 100 == 0:
        wb.save('cardlist.xlsx')
        print('excel saved.')
    time.sleep(2)
  wb.save('cardlist.xlsx')
  print('pageCheck end.')
    

execute()


