import requests
import datetime
import openpyxl
import time
import re
from bs4 import BeautifulSoup
from urllib.request import urlopen

load_url = "https://www.cardrush-pokemon.jp/product/"
debug = True

#start文字～end文字までを切り取る
#[xxx]ならxxx
#引数src：str、start：str、end：str
def cutoutStr(src, start, end):
  if start not in src or end not in src:
    return None
  idx1 = src.find(start)
  idx2 = src.rfind(end)
  dest = src[idx1+1:idx2]
  return dest

#カード番号を抜く
#引数src：str
def getCardNumber(src):
  return str(cutoutStr(src, '{', '}'))

#純粋なカード名を抜く
#引数src：str
def getCardName(src):
  if '〕' in src:
    idx = src.find('〕')
    src = src[idx+1:]
  if '☆' in src:
    idx = src.rfind('☆')
    src = src[idx+1:]
  if '【' in src:
    idx = src.find('【')
    src = src[:idx]
  return src

#収録パックを抜く
#引数src：str
def getPack(src):
  if src.find(class_="model_number_value") is None:
    return 'その他'
  pack = src.find(class_="model_number_value").text
  if ']' in pack:
    idx = pack.rfind(']')
    pack = pack[idx+1:]
  return pack

#レアリティを抜く
#引数src：str
def getRareRarity(src):
  return str(cutoutStr(src, '【','】'))
  
#SAかどうかを抜く
#引数src：str
def isSa(src):
  return '(SA)' in src

#状態を抜く
#引数src：str
def getStatus(src):
  return str(cutoutStr(src, '〔','〕'))

#SALE品かどうかを抜く
#引数src：str
def isSale(src):
  return 'SALE' in src

#重みを抜く
#引数src：soup
def getWeight(src):
  option = src.find(class_="delivery_option")
  if option is None: 
    print("重みがのん！")
    return None
  weight = src.find(class_="delivery_option").text
  weight = weight.replace('重み:','')
  weight = weight.rstrip()
  weight = weight.strip()
  return int(weight)


#メイン実行
def execute():
  global load_url
  global debug
  #Excel読み込み
  wb = openpyxl.load_workbook("cardlist.xlsx")
  ws = wb["Sheet1"]

  file = open('notExistList.txt', 'a', encoding='UTF-8')

  #現在日時取得
  dt_now = datetime.datetime.now()
  today = dt_now.strftime('%y/%m/%d %H:%M:%S')
  print(today)

  lineCnt = 4329
  for num in range(4959,25000):
    html = requests.get(str(load_url) + str(num))
#    html = requests.get("https://www.cardrush-pokemon.jp/product/7")
    soup = BeautifulSoup(html.content, "html.parser")
    if soup.find(class_="nftitle_bottom") is None:
      title = soup.find("title").text
      if debug: print(title)
      ws.cell(row=lineCnt, column=1, value=num)
      ws.cell(row=lineCnt, column=2, value=today)
      ws.cell(row=lineCnt, column=3, value=title)
      ws.cell(row=lineCnt, column=4, value=getCardName(title))
      ws.cell(row=lineCnt, column=5, value=getPack(soup))
      ws.cell(row=lineCnt, column=6, value=getRareRarity(title))
      ws.cell(row=lineCnt, column=7, value=getCardNumber(title))
      ws.cell(row=lineCnt, column=8, value=isSa(title))
      ws.cell(row=lineCnt, column=9, value=getStatus(title))
      ws.cell(row=lineCnt, column=10, value=isSale(title))
      ws.cell(row=lineCnt, column=11, value=getWeight(soup))
      
      if debug: print("getCardName(title): "+str(getCardName(title)))
      if debug: print("getPack(soup): "+str(getPack(soup)))
      if debug: print("getRareRarity(title): "+str(getRareRarity(title)))
      if debug: print("getCardNumber(title): "+str(getCardNumber(title)))
      if debug: print("isSa(title): "+str(isSa(title)))
      if debug: print("getStatus(title): "+str(getStatus(title)))
      if debug: print("isSale(title): "+str(isSale(title)))
      if debug: print("getWeight(soup): "+str(getWeight(soup)))
      lineCnt += 1
      wb.save('cardlist.xlsx')
      if lineCnt % 100 == 0:
        wb.save('cardlist.xlsx')
        print('excel saved.')
      time.sleep(1)
    else:
      file.write(str(num) + '\n')

  wb.save('cardlist.xlsx')
  print('pageCheck end.')


    

execute()


