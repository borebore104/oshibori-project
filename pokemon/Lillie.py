import requests
import datetime
import openpyxl
import time
from bs4 import BeautifulSoup

load_url = "https://www.cardrush-pokemon.jp/product/"
date = ""
rowIndex = 0
ws = None
def execute():
  global load_url
  global date
  global rowIndex
  global ws
  date = datetime.datetime.now()
  wb = openpyxl.load_workbook("cardlist.xlsx")
  ws = wb["Sheet1"]

  #日付記入
  colIndex = 0
  for i in range(10000):
    if i == 0:
      continue
    if ws.cell(row=1, column=i).value is None:
      ws.cell(row=1, column=i).value = date.strftime('%y/%m/%d %H:%M:%S')
      wb.save("cardlist_" + str(date.strftime('%y%m%d')) + ".xlsx")
      colIndex = i
      break
  print("---------- colIndex:" + str(colIndex) +" ----------")

  #価格記入  
  rowIndex = 1
  for row in ws["A2:A30000"]:
    rowIndex +=1
    print(type(ws.cell(row=rowIndex, column=1).value))
    print(ws.cell(row=rowIndex, column=1).value)

    if ws.cell(row=rowIndex, column=1).value == None:
      wb.save("cardlist_" + str(date.strftime('%y%m%d')) + ".xlsx")
      break
    price = ditectPrice(str(load_url) + str(row[0].value))
    print(price)
    ws.cell(row=rowIndex, column=colIndex).value = price
    time.sleep(1)
    if rowIndex % 100 == 0:
      wb.save("cardlist_" + str(date.strftime('%y%m%d')) + ".xlsx")
      print('excel saved.')
  wb.save("cardlist_" + str(date.strftime('%y%m%d')) + ".xlsx")
  print("---------- Lillie end ----------")
  
#  #カードリスト 外部から読み込む
#  list = {"21646","13534","1586","5249","14730","5183","5202","4373","5177","5208","1592","8781","15941"}
#  dt_now = datetime.datetime.now()
#  print(dt_now.strftime('%y/%m/%d %H:%M:%S'))
#
#  for item in list:
#    ditectCard(str(load_url) + str(item))
#    time.sleep(1)

#価格記入
def ditectPrice(url):
  global rowIndex
  global ws
  html = requests.get(url)
  soup = BeautifulSoup(html.content, "html.parser")
  print(soup.find("title").text)
  ws.cell(row=rowIndex, column=2).value = soup.find("title").text
  print(convertPrice(soup.find(id = "pricech").text))
  if soup.find(class_="detail_section stock") is None:
    return 0
  elif str(convertStock(soup.find(class_="detail_section stock").text)) == str("×"):
    print("---------- detail_section stock is none ----------")
    return 0
  else:
    return int(convertPrice(soup.find(id = "pricech").text))


def ditectCard(url):
  html = requests.get(url)
  soup = BeautifulSoup(html.content, "html.parser")
  print("--------------------------------------------------")
  print(soup.find("title").text)
  print(convertPrice(soup.find(id = "pricech").text))
  print(convertStock(soup.find(class_="detail_section stock").text))

def convertPrice(price):
  return price.replace(',','').replace('円','')

def convertStock(stock):
  return str(stock).replace('在庫数','').replace('枚','').rstrip().strip()

execute()


