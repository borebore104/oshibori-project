#Copyright 2021 Oshibori@2tenjin
import requests
import datetime
import os
import shutil

#DLするULR(ファイル名抜き)
url = 'https://s3.amazonaws.com/JP_AM/shinausu/'

#DLするファイル名
fileNames=['Book_recommend_item_list.xlsx','Music_Recommended_Item_List.xlsx','VD_Recommended_Item_List.xlsx','VG_Recommended_Item_List.xlsx','SW_Recommended_Item_List.xlsx','TH_Recommended_Item_List.xls','CE_Recommended_Item_List.xlsx','Camera_Recommended_Item_List.xlsx','PC_Recommended_Item_List.xlsx','Kitchen_Recommended_Item_List.xlsx','Home_Recommended_Item_List.xlsx','MI_Recommended_Item_List.xlsx','OP_Recommended_Item_List.xlsx','60_Recommended_Item_List.xls','Automotive_recommended_item_list.xlsx','SG_Recommended_Item_List.xlsx','BISS_Recommended_Item_list.xlsx','Apparel_Recommended_Item_List.xlsx','Shoes_Recommended_Item_List.xlsx','JW_suisho.xlsx','WT_suisho.xlsx','Pets_recommend_item_list.xlsx','Baby_Recommended_Item_List.xlsx','hpc_recommend_item_list.xlsx','beauty_recommend_item_list.xlsx','Grocery_recommend_item_list.xlsx','Wine_recommend_item_list.xlsx']

dlFolder = ''

def dlFiles():
  print('dlFiles')
  global dlFolder

  for fileName in fileNames:
    fullUrl=url+fileName
    distFileName = dlFolder + fileName
    print('dlFolder:' + dlFolder + 'fileName:' + fileName + 'distFileName:' + distFileName)

    urlData = requests.get(fullUrl).content
    print(distFileName)

    with open(distFileName ,mode='wb') as f: # wb でバイト型を書き込める
      f.write(urlData)

def archiveFile(name):
  print('archiveFile')
  shutil.make_archive(name, 'zip', root_dir=name)

#タイムスタンプのYYYYMMDDを返す
def getTimeStamp():
  today = datetime.date.today()
  strDate = today.strftime('%Y%m%d')
  print('getTimeStamp:' + strDate)
  return strDate

#DL先フォルダを作成する
def createDLFolder(name):
  global dlFolder
  
  print('createDLFolder name:' + name)
  os.mkdir(name)
  dlFolder = name
  #空文字じゃない場合はパスが入ってるから末尾に\を入れる
  if dlFolder != '':
    dlFolder += '\\'

createDLFolder(getTimeStamp())
dlFiles()
archiveFile(getTimeStamp())

