import openpyxl

icon_raku = "http://nana.hippy.jp/otoku/wp-content/uploads/2021/08/rakuten.png"
icon_ama = "http://nana.hippy.jp/otoku/wp-content/uploads/2021/08/Amazon.png"
ama_afiid = "/ref=nosim?tag=yiu0k2a2r7-22"

debug = True

def createImage(url):
  tag = '<img src=\"' + url + '\" style=\"max-width: 90%\">'
  return str(tag)

def createAmaLink(price, url):
  global icon_ama
  global ama_afiid
  tag = '\\ ' + str("{:,}".format(price)) + '<br><a href=\"' + url + ama_afiid + '\" target=\"_blank\" rel=\"noopener\"> <img src=\"' + icon_ama + '\"></a>'
  return str(tag)

def createRakuLink(price, url):
  global icon_raku
  tag = '\\ ' + str("{:,}".format(price)) + '<br><a href=\"' + url + '\" target=\"_blank\" rel=\"noopener\"> <img src=\"' + icon_raku + '\"></a>'
  return str(tag)


def createRow(title, imageUrl, amaPrice, amaUrl, rakuPrice, rakuUrl):
  tag = '<tr><td align=\"left\" valign=\"middle\">' + title + '</td><td>' + createImage(imageUrl) + '</td><td align=\"middle\" valign=\"middle\">' + createAmaLink(amaPrice, amaUrl) + '</td><td align=\"middle\" valign=\"middle\">' + createRakuLink(rakuPrice, rakuUrl) + '</td></tr>'
  return str(tag)

def createTableHeader():
  tag = '<table style=\"width:100%; border-collapse: collapse\"  border=\"1\">'
  tag += '<thead>'
  tag += '<tr>'
  tag += '<td style=\"width:52%\" align=\"middle\" valign=\"middle\">タイトル</td>'
  tag += '<td style=\"width:8%\" align=\"middle\" valign=\"middle\">イメージ</td>'
  tag += '<td style=\"width:20%\" align=\"middle\" valign=\"middle\">Amazon</td>'
  tag += '<td style=\"width:20%\" align=\"middle\" valign=\"middle\">楽天</td>'
  tag += '</tr>'
  tag += '</thead>'
  tag += '<tbody>'
  return str(tag)

def createTableHooter():
  tag = '</tbody>'
  tag += '</table>'
  return str(tag)



def createTable():
  wb = openpyxl.load_workbook("source.xlsx")
  ws = wb["Sheet1"]
  print('createHeader end.')

  file = open('Table.html', 'a', encoding='UTF-8')
  file.write(createTableHeader())
  for rowCnt in range(1,1000):
    print(rowCnt)
    print(ws['A' + str(rowCnt)].value)
    print(ws['B' + str(rowCnt)].value)
    print(ws['C' + str(rowCnt)].value)
    print(ws['D' + str(rowCnt)].value)
    print(ws['E' + str(rowCnt)].value)
    print(ws['F' + str(rowCnt)].value)
    row = createRow(ws['A' + str(rowCnt)].value,ws['B' + str(rowCnt)].value,ws['C' + str(rowCnt)].value,ws['D' + str(rowCnt)].value,ws['E' + str(rowCnt)].value,ws['F' + str(rowCnt)].value)
    file.write(row)

  file.write(createTableHooter())


createTable()









